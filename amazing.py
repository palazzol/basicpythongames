from __future__ import print_function
import platform
VERSION = platform.python_version()
if VERSION[0] == '2':
    input = raw_input

import random
                
width = int(input('width?'))
length = int(input('length?'))
    
used = [[0]*(width) for col in range(length)]
walls = [[0]*(width) for col in range(length)]

GO_LEFT,GO_UP,GO_RIGHT,GO_DOWN=range(4)

enter_col=random.randint(0,width-1)
row,col=0,enter_col
count=1
used[row][col]=count
count=count+1

while count!=width*length+1:
    possible_dirs = [GO_LEFT,GO_UP,GO_RIGHT,GO_DOWN]
    if col==0 or used[row][col-1]!=0:
        possible_dirs.remove(GO_LEFT)
    if row==0 or used[row-1][col]!=0:
        possible_dirs.remove(GO_UP)
    if col==width-1 or used[row][col+1]!=0: 
        possible_dirs.remove(GO_RIGHT)
    if row==length-1 or used[row+1][col]!=0:
        possible_dirs.remove(GO_DOWN)   

    if len(possible_dirs)!=0:
        direction=random.choice(possible_dirs) 
        if direction==GO_LEFT:
            col=col-1
            walls[row][col]=2
        elif direction==GO_UP:
            row=row-1 
            walls[row][col]=1
        elif direction==GO_RIGHT:
            walls[row][col]=walls[row][col]+2    
            col=col+1
        elif direction==GO_DOWN:
            walls[row][col]=walls[row][col]+1
            row=row+1
        used[row][col]=count
        count=count+1
    else:
        while True:
            if col!=width-1:
                col=col+1
            elif row!=length-1:
                row,col=row+1,0
            else:
                row,col=0,0
            if used[row][col]!=0:
                break

col=random.randint(0,width-1)
row=length-1
walls[row][col]=walls[row][col]+1
    
for col in range(width):
    if col==enter_col:
        print('.  ',end='')
    else:
        print('.--',end='')
print('.')
for row in range(length):
    print('I',end='')
    for col in range(width):
        if walls[row][col]<2:
            print('  I',end='')
        else:
            print('   ',end='')
    print()
    for col in range(width):
        if walls[row][col]==0 or walls[row][col]==2:
            print(':--',end='')
        else:
            print(':  ',end='')    
    print('.')

            
        
        
        